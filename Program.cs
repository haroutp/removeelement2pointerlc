﻿using System;

namespace RemoveElement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static int RemoveElement(int[] nums, int val) {
            int k = 0;
            
            for(int i = 0; i < nums.Length; i++){
                if(nums[i] != val){
                    nums[k] = nums[i];
                    k++;
                }
            }
            return k;
        }
    }
}
